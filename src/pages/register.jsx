import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import { red, green } from 'material-ui/colors'
import IconButton from 'material-ui/IconButton'
import Input, { InputLabel, InputAdornment } from 'material-ui/Input'
import Button from 'material-ui/Button'
import { FormControl, FormHelperText } from 'material-ui/Form'
import Visibility from 'material-ui-icons/Visibility'
import VisibilityOff from 'material-ui-icons/VisibilityOff'
import SvgIcon from 'material-ui/SvgIcon';
import Typography from 'material-ui/Typography/Typography';
import { register } from '../actions/actions-auth'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  withoutLabel: {
    marginTop: theme.spacing.unit * 3,
  },
  button: {
    width: '100%',
    marginBottom: 10
  },
  googleIcon: {
    fill: red[500],
  },
});

class Register extends React.Component {
  state = {
    email: '',
    password: '',
    showPassword: false,
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPasssword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  handleRegister = () => {
    this.props.register(this.state.email, this.state.password)
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <FormControl fullWidth={true} className={classNames(classes.formControl, classes.withoutLabel)}>
        <InputLabel htmlFor="email">Email</InputLabel>
          <Input
            type='email'
            id="email"
            value={this.state.email}
            onChange={this.handleChange('email')}
          />
        </FormControl>
        <FormControl fullWidth={true} className={classes.formControl}>
          <InputLabel htmlFor="password">Password</InputLabel>
          <Input
            id="password"
            type={this.state.showPassword ? 'text' : 'password'}
            value={this.state.password}
            onChange={this.handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  onClick={this.handleClickShowPasssword}
                  onMouseDown={this.handleMouseDownPassword}
                >
                  {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <FormControl fullWidth={true} className={classes.formControl}>
          <InputLabel>Password Again</InputLabel>
          <Input
            type={this.state.showPassword ? 'text' : 'password'}
            value={this.state.passwordTest}
            onChange={this.handleChange('passwordTest')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  onClick={this.handleClickShowPasssword}
                  onMouseDown={this.handleMouseDownPassword}
                >
                  {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        <Button raised color="primary" className={classes.button} onClick={this.handleRegister}>
          Register
        </Button>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      register
    },
    dispatch
  )
}

export default withStyles(styles)(connect(undefined, mapDispatchToProps)(Register));
