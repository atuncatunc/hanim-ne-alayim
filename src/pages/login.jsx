import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import { red, green } from 'material-ui/colors'
import IconButton from 'material-ui/IconButton'
import Input, { InputLabel, InputAdornment } from 'material-ui/Input'
import Button from 'material-ui/Button'
import { FormControl, FormHelperText } from 'material-ui/Form'
import Visibility from 'material-ui-icons/Visibility'
import VisibilityOff from 'material-ui-icons/VisibilityOff'
import SvgIcon from 'material-ui/SvgIcon'
import Typography from 'material-ui/Typography/Typography'
import { login, watchAuthStateChange } from '../actions/actions-auth'

const FacebookIcon = props => (
  <SvgIcon {...props}>
    <path d="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" />
  </SvgIcon>
)

const GoogleIcon = props => (
  <SvgIcon {...props} viewBox='0 0 24 24'>
    <path fill="#4D8CF5" d="M23,11H21V9H19V11H17V13H19V15H21V13H23M8,11V13.4H12C11.8,14.4 10.8,16.4 8,16.4C5.6,16.4 3.7,14.4 3.7,12C3.7,9.6 5.6,7.6 8,7.6C9.4,7.6 10.3,8.2 10.8,8.7L12.7,6.9C11.5,5.7 9.9,5 8,5C4.1,5 1,8.1 1,12C1,15.9 4.1,19 8,19C12,19 14.7,16.2 14.7,12.2C14.7,11.7 14.7,11.4 14.6,11H8Z" />
  </SvgIcon>
)
const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  withoutLabel: {
    marginTop: theme.spacing.unit * 3,
  },
  button: {
    width: '100%',
    marginBottom: 10
  },
  googleIcon: {
    fill: red[500],
  },
});

class Login extends React.Component {
  state = {
    email: '',
    showPassword: false,
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPasssword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  }

  componentWillMount = () => {
    this.props.watchAuthStateChange()
  }

  handleLogin = () => {
    const { email, password } = this.state
    this.props.login(email, password)
  }

  render() {
    const { classes, auth } = this.props;

    return auth.isAuthenticated
      ? <Redirect to='/shopping-lists' />
      : (
        <div className={classes.root}>
          <FormControl fullWidth={true} className={classNames(classes.formControl, classes.withoutLabel)}>
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input
              type='email'
              id="email"
              value={this.state.email}
              onChange={this.handleChange('email')}
            />
            <FormHelperText>Email</FormHelperText>
          </FormControl>
          <FormControl fullWidth={true} className={classes.formControl}>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              type={this.state.showPassword ? 'text' : 'password'}
              value={this.state.password}
              onChange={this.handleChange('password')}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    onClick={this.handleClickShowPasssword}
                    onMouseDown={this.handleMouseDownPassword}
                  >
                    {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <Button raised color="primary" className={classes.button} onClick={this.handleLogin}>
            Login
        </Button>
          <Button raised color="primary" className={classes.button}>
            <FacebookIcon />
            Facebook
        </Button>
        </div>
      );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      login,
      watchAuthStateChange
    },
    dispatch
  )
}


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Login));
