import {
  SHOPPING_ITEM_ADD,
  SHOPPING_ITEM_FETCH,
  SHOPPING_ITEM_REMOVE
} from '../actions/types'

const initialState = {
  shoppingItems: []
}

export const shoppingListReducer = (state = initialState, action) => {
  switch(action.type) {
    case SHOPPING_ITEM_ADD:
      return {shoppingItems: [...state.shoppingItems, ...action.payload.data]}
    case SHOPPING_ITEM_FETCH:
      return {shoppingItems: action.payload.data}
    default:
      return state
  }
}
