import { combineReducers } from 'redux';
import { shoppingListReducer } from './reducer-shopping-list'
import { shoppingListsReducer } from './reducer-shopping-lists'
import { authReducer } from './reducer-auth'

export const rootReducer = combineReducers({
  shoppingList: shoppingListReducer,
  shoppingLists: shoppingListsReducer,
  auth: authReducer
})
