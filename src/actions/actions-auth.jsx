import firebase from 'firebase'
import { config } from './firebase-app-config'

import {
  AUTH_LOGIN_FAILURE,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT,
  AUTH_REGISTER_SUCCESS
} from './types'

const firebaseApp = firebase.initializeApp(config);

export const login = (email, password) => dispatch => {
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(res => {
      dispatch({type: AUTH_LOGIN_SUCCESS, payload: res})
    })
    .catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
}

export const watchAuthStateChange = () => dispatch => {
  firebase
    .auth()
    .onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        const payload = {
          displayName: user.displayName,
          email: user.email,
          emailVerified: user.emailVerified,
          photoURL: user.photoURL,
          isAnonymous: user.isAnonymous,
          uid: user.uid,
          providerData: user.providerData
        }
        dispatch({type: AUTH_LOGIN_SUCCESS, payload})
        // ...
      } else {
        // User is signed out.
        // ...
        dispatch({type: AUTH_LOGOUT, payload: {}})
      }
    })
}

export const register = (email, password) => dispatch => {
  firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then( res => {
      const {
        displayName,
        email,
        emailVerified,
        photoURL,
        isAnonymous,
        uid,
        providerData
      } = res;

      const data = {displayName, email, emailVerified, photoURL, isAnonymous, uid, providerData}
      dispatch({type: AUTH_REGISTER_SUCCESS, payload: {data}})
    })
    .catch(error => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
}
