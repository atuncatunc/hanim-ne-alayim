import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Card, { CardContent, CardActions } from 'material-ui/Card'
import Collapse from 'material-ui/transitions/Collapse'
import Grid from 'material-ui/Grid'
import { green, orange } from 'material-ui/colors'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import withStyles from 'material-ui/styles/withStyles'
import DoneAll from 'material-ui-icons/DoneAll'
import ExpandLess from 'material-ui-icons/ExpandLess'
import ExpandMore from 'material-ui-icons/ExpandMore'

import ShoppingItem from './shopping-item';

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
    color: theme.palette.text.secondary,
  },
  pos: {
    marginBottom: 12,
    color: theme.palette.text.secondary,
  },
  list: {
    width: '100%'
  },
  done: {
    color: green[500]
  },
  dont: {
    color: orange[500]
  }
})

class ShoppingList extends Component {

  state = {
    active: true,
    passive: true
  }

  handleActiveClick = () => {
    this.setState({active: !this.state.active})
  }

  handlePassiveClick = () => {
    this.setState({passive: !this.state.passive})
  }

  render() {
    const { list, classes, shoppingList, id } = this.props

    return (
      <Grid container className={classes.grid}>
        <Grid item xs={12}>
          <List className={classes.list}>
            <ListItem button onClick={this.handleActiveClick}>
              <ListItemIcon>
                <DoneAll className={classes.dont}/>
              </ListItemIcon>
              <ListItemText inset primary="Alınacak" />
              {this.state.active ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse component="li" in={this.state.active}>
              <List>
                {
                  shoppingList.filter(item => !item.value.done).map(item =>
                    <ShoppingItem key={item.key} item={item} listId={id} />
                  )
                }
              </List>
            </Collapse>
            <Divider />
            <ListItem button onClick={this.handlePassiveClick}>
              <ListItemIcon>
                <DoneAll className={classes.done}/>
              </ListItemIcon>
              <ListItemText inset primary="Alındı" />
              {this.state.passive ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse component="li" in={this.state.passive}>
              <List>
                {
                  shoppingList.filter(item => item.value.done).map(item =>
                    <ShoppingItem key={item.key} item={item} listId={id} />
                  )
                }
              </List>
            </Collapse>
          </List>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    shoppingList: state.shoppingList.shoppingItems
  }
}

export default withStyles(styles)(connect(mapStateToProps, undefined)(ShoppingList))
